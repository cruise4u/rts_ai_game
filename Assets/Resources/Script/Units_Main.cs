﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units_Main : MonoBehaviour
{



   
    public static GameObject Friendly;
    public static GameObject Ground;
    public static GameObject Enemy;
    public static GameObject MySelf;
    public void Check_Team()
    {
        if (MySelf == GameObject.FindGameObjectWithTag("Team 1"))
        {
            Friendly = GameObject.FindGameObjectWithTag("Team 1");
            Enemy = GameObject.FindGameObjectWithTag("Team 2");
        }
        else
        {
            Friendly = GameObject.FindGameObjectWithTag("Team 2");
            Enemy = GameObject.FindGameObjectWithTag("Team 1");
        }
    }

    private bool EnemyIsSelected = false;
    private bool IsSelecting = false;
    private bool IsMoving = false;
    private bool ItCanAttack = false;
    public static bool selecting;

    public static List<GameObject> UnitsList = new List<GameObject>();

    public static Vector3 Select_Mouseposition = new Vector3();
    public static Vector3 DirectionToClick = new Vector3();
    public static Vector3 NewUnitPosition = new Vector3();

    private Texture2D boxTxt;
    private Rect BoxRect;
    private Rect SelectionBox;
    private LineRenderer SquareLine;


    // Functions ..

    

    public static Rect DrawRect(Vector3 ScreenPos_1, Vector3 ScreenPos_2)
    {
        // Move origin from bottom left to top left
        ScreenPos_1.y = Screen.height - ScreenPos_1.y;
        ScreenPos_2.y = Screen.height - ScreenPos_2.y;

        // Calculate corners
        var topLeft = Vector3.Min(ScreenPos_1, ScreenPos_2);
        var bottomRight = Vector3.Max(ScreenPos_1, ScreenPos_2);

        // Create Rect
        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public static Bounds GetSelectionBounds(Camera viewPortCam, Vector3 SP1, Vector3 SP2)
    {
        var vec1 = viewPortCam.ScreenToViewportPoint(SP1);
        var vec2 = viewPortCam.ScreenToViewportPoint(SP2);
        var min = Vector3.Min(vec1, vec2);
        var max = Vector3.Max(vec1, vec2);

        min.z = viewPortCam.nearClipPlane;
        max.z = viewPortCam.farClipPlane;

        var boundsCreate = new Bounds();

        boundsCreate.SetMinMax(min, max);
        return boundsCreate;
    }

    private bool CheckWithinRect(GameObject Game_Obj)
    {
        if (!IsSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds =
           GetSelectionBounds(camera, Select_Mouseposition, Input.mousePosition);

        return viewportBounds.Contains(
            camera.WorldToViewportPoint(Game_Obj.transform.position));

    }

  
    private bool Check_RayObject(GameObject character)
    {
        if (!IsSelecting)
            return false;

        bool checkCollision = new bool();

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if(hit.collider == character.GetComponent<Collider>())
            {
                checkCollision = true;
            }
            else
            {
                checkCollision = false;
            }
        }

        return checkCollision;
    }


    // Use this for initialization
    void Start()
    {
        MySelf = gameObject;
        SquareLine = GetComponent<LineRenderer>();
        boxTxt = new Texture2D(1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        Check_Team();
        SquareLine.enabled = false;


        if(Input.GetMouseButton(0))
        {


            IsSelecting = true;
            if(IsSelecting==true)
            {
                Select_Mouseposition = Input.mousePosition;
                SquareLine.enabled = true;
            }
        }

        if (Input.GetMouseButtonUp(0) && Check_RayObject(Ground))
        {
        IsSelecting = false;
        SquareLine.enabled = false;
        }
        





    }




    void OnGUI()
    {
        if (IsSelecting == true)
        {
            SelectionBox = DrawRect(Select_Mouseposition, Input.mousePosition);
            GUI.DrawTexture(SelectionBox, boxTxt);
        }
    }
}
