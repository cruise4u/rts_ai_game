﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AI_Controller : MonoBehaviour
{

    Units_Stats AIStats;
    Units_Stats PlayerStats;
    Units_UI AI_UI;
    TestWeapon AIWeapon;
    NavMeshAgent AI_Agent;


    private GameObject Self;
    private GameObject Friendly;
    private GameObject Enemy;
    private GameObject Ground;
    private Animator AC;


    private float attackrange = new float();

    private float attackSpeed = new float();
    private float attackCooldown = new float();

    public static Vector3 Select_Mouseposition = new Vector3();
    public static Vector3 DirectionToClick = new Vector3();
    public static Vector3 NewUnitPosition = new Vector3();

    public static RaycastHit hit;
    public static Ray ray;

    bool IsSelecting = new bool();

    // Movement functions 

    void AIToPoint()
    {
        if (1>0)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider == Ground.GetComponent<Collider>() || Friendly.GetComponent<Collider>())
                {
                    AI_Move(hit.point);
                }
                else if (hit.collider == Enemy.GetComponent<Collider>())
                {
                    AI_Move(hit.point);
                }
            }
        }
    }

    void Attack_Player()
    {
        if (attackCooldown <= 0.0f)
        {
            AIWeapon.AttackMotion();
            PlayerStats.DamageDone(100);
            attackCooldown = 1f / attackSpeed;
        }
    }

    void AI_Move(Vector3 point)
    {
        AI_Agent.SetDestination(point);
    }

    private bool RaySelect(GameObject InteractableObj)
    {
        if (!IsSelecting)
            return false;

        var boolean = new bool();

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider == InteractableObj.GetComponent<Collider>())
            {
                boolean = true;
            }
            else
            {
                boolean = false;
            }
        }

        return boolean;
    }

    public GameObject IsDestroyed(GameObject target)
    {
        if (target != null)
        {
            target = GameObject.FindGameObjectWithTag("Team 2");
        }
        else
        {
            target = null;
        }
        return target;
    }

    public void TeamManager()
    {
        if (gameObject == GameObject.FindGameObjectWithTag("Team 1"))
        {
            Enemy = GameObject.FindGameObjectWithTag("Team 2");
            Friendly = GameObject.FindGameObjectWithTag("Team 1");
            Ground = GameObject.FindGameObjectWithTag("Plane");
        }
        else
        {
            Enemy = GameObject.FindGameObjectWithTag("Team 1");
            Friendly = GameObject.FindGameObjectWithTag("Team 2");
            Ground = GameObject.FindGameObjectWithTag("Plane");
        }
    }




    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
