﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class Units_Controller : MonoBehaviour
{
    // Using other script inside this one..
    NavMeshAgent SpecialAgent;
    AnimatorStateInfo CurrentState;

    Units_Stats CharacterStats;


    // Local Variables..

    float velocity;


    // Game objects defenitions .. :


    private GameObject Self;

    private GameObject Friendly;
    private GameObject Enemy;
    private GameObject Ground;



    // booleans to state different conditions
    private bool EnemyIsSelected = false;
    private bool IsSelecting = false;
    private bool IsMoving = false;



    // Rectangle components to draw in GUI
    private Texture2D boxTxt;
    private Rect BoxRect;
    private Rect SelectionBox;

    private LineRenderer SquareLine;

    private Animator anim;

    public static bool selecting;
    public static List<GameObject> UnitsSelection = new List<GameObject>();

    public static Vector3 Select_Mouseposition = new Vector3();
    public static Vector3 DirectionToClick = new Vector3();
    public static Vector3 NewUnitPosition = new Vector3();


    public static RaycastHit hit;
    public static Ray ray;


    // Public Functions..

    public static Rect DrawRect(Vector3 ScreenPos_1, Vector3 ScreenPos_2)
    {
        // Move origin from bottom left to top left
        ScreenPos_1.y = Screen.height - ScreenPos_1.y;
        ScreenPos_2.y = Screen.height - ScreenPos_2.y;

        // Calculate corners
        var topLeft = Vector3.Min(ScreenPos_1, ScreenPos_2);
        var bottomRight = Vector3.Max(ScreenPos_1, ScreenPos_2);

        // Create Rect
        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public static Bounds GetSelectionBounds(Camera viewPortCam, Vector3 SP1, Vector3 SP2)
    {
        var vec1 = viewPortCam.ScreenToViewportPoint(SP1);
        var vec2 = viewPortCam.ScreenToViewportPoint(SP2);
        var min = Vector3.Min(vec1, vec2);
        var max = Vector3.Max(vec1, vec2);

        min.z = viewPortCam.nearClipPlane;
        max.z = viewPortCam.farClipPlane;

        var boundsCreate = new Bounds();

        boundsCreate.SetMinMax(min, max);
        return boundsCreate;
    }




    // Movement functionality.. 
    //public Vector3 MoveToPosition(List<GameObject> UnitsSelection)
    //{
    //    Vector3 newPosition = new Vector3();

    //    foreach (GameObject obj in UnitsSelection)
    //    {
    //        var ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane));
    //        if (Physics.Raycast(ray, out hit, 1000))
    //        {
    //            velocity = 0.1f;
    //            DirectionToClick = hit.point;
    //            DirectionToClick.y = 0;
    //            newPosition = DirectionToClick - obj.transform.position;
    //            var RotateObject = Quaternion.FromToRotation(obj.transform.forward, newPosition);
    //            obj.gameObject.transform.rotation *= RotateObject;
    //            IsMoving = true;
    //        }
    //    }
    //    return newPosition;
    //}


    public float DistanceToPoint(Vector3 vec_1, Vector3 vec_2)
    {
        float DistanceToPoint = new float();
        DistanceToPoint = Vector3.Distance(vec_1, vec_2);
        return DistanceToPoint;
    }

    void ContainsOnList()
    {
        if (CheckWithinRect(Self) || RaySelect(Self))
        {
            UnitsSelection.Add(Self);
        }

        if (UnitsSelection.Contains(Self) && Input.GetMouseButton(0))
        {
            UnitsSelection.Remove(Self);
        }

        // And consequently highlighted..
        if (UnitsSelection.Contains(Self))
        {
            SquareLine.enabled = true;
        }
        else
        {
            SquareLine.enabled = false;
        }



    }

    float attackrange = new float();


    void MoveToPoint()
    {
        NeutralPoint();
        //EnemyPoint();
    }




    void NeutralPoint()
    {
        if (UnitsSelection.Contains(Self))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider == Ground.GetComponent<Collider>() || Friendly.GetComponent<Collider>())
                {
                        AgentMove(hit.point);
                        anim.SetTrigger("Movy!");
                }
            }
        }
    }

    //void EnemyPoint()
    //{
    //    if (UnitsSelection.Contains(Self))
    //    {
    //        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    //        if (Physics.Raycast(ray, out hit, 1000))
    //        {
    //            if (hit.collider == Enemy.GetComponent<Collider>())
    //            {
    //                AgentAttack(hit.point);
    //            }
    //        }
    //    }
    //}


    public void Die(GameObject gameobject)
    {
        GameObject unit = gameobject;
        Destroy(unit);
    }

    private void AgentMove(Vector3 point)
    {
        SpecialAgent.SetDestination(point);

    }

    private void Attack()
    {

    }

    // Selection..
    
    private bool RaySelect(GameObject InteractableObj)
    {
        if (!IsSelecting)
            return false;

        var boolean = new bool();

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider == InteractableObj.GetComponent<Collider>())
            {
                boolean = true;
            }
            else
            {
                boolean = false;
            }
        }

        return boolean;
    }

    private bool CheckWithinRect(GameObject Game_Obj)
    {
        if (!IsSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds =
           GetSelectionBounds(camera, Select_Mouseposition, Input.mousePosition);

        return viewportBounds.Contains(
            camera.WorldToViewportPoint(Game_Obj.transform.position));

    }

    CharacterController CharControl;

    void Awake()
    {
       
    }

    void Start()
    {
        IsMoving = false;
        SpecialAgent = GetComponent<NavMeshAgent>();
        attackrange = 1.0f;

        CharacterStats = GetComponent<Units_Stats>();


        Friendly = GameObject.FindGameObjectWithTag("Team 1");
        Enemy = GameObject.FindGameObjectWithTag("Team 2");
        Ground = GameObject.FindGameObjectWithTag("Plane");
        Self = gameObject;


        CharControl = GetComponent<CharacterController>();
        // Animator Component 
        anim = GetComponent<Animator>();

        // Components for GUI and Line Renderer
        boxTxt = new Texture2D(1, 1);
        SquareLine = GetComponent<LineRenderer>();
    }



    void Update()
    {
        SpecialAgent.baseOffset = 0.0f;
        SpecialAgent.height = 0.0f;
        
        CurrentState = anim.GetCurrentAnimatorStateInfo(0);


        // If we press the left mouse button, save mouse location and begin selection
        if (Input.GetMouseButtonDown(0))
        {
            IsSelecting = true;
            if (IsSelecting == true)
            {
                Select_Mouseposition = Input.mousePosition;
            }
        }

        ContainsOnList();

        // If we have our unit selected and press Right-Click Mouse Button on a target..

        if (Input.GetMouseButtonDown(1))
        {
            MoveToPoint();
        }

        if (Input.GetMouseButtonUp(0))
        {
            IsSelecting = false;
        }


        if (SpecialAgent.remainingDistance <= SpecialAgent.stoppingDistance && CurrentState.IsName("Walk"))
        {
            anim.SetTrigger("NotMovy!");
        }


    }


    // GUI
    void OnGUI()
    {
        if (IsSelecting == true)
        {
            SelectionBox = DrawRect(Select_Mouseposition, Input.mousePosition);
            GUI.DrawTexture(SelectionBox, boxTxt);
        }


    }
}

