﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Units_Stats))]
public class Units_UI : MonoBehaviour {

    public GameObject UIprefab;
    public Transform Target;

    Transform UI;
    Image HealthSlider;

    Transform cam;



    public void OnHealthChanged(int maxHealth,int currentHealth)
    {
        if(UI != null)
        {
            UI.gameObject.SetActive(true);


            float healthPercent = currentHealth / (float)maxHealth;
            HealthSlider.fillAmount = healthPercent;
            if (currentHealth <= 0)
            {
                Destroy(UI.gameObject);
            }
        }

    }


    // Use this for initialization
    void Start()
    {
        cam = Camera.main.transform;

        foreach(Canvas canvas in FindObjectsOfType<Canvas>())
        {
            if(canvas.renderMode == RenderMode.WorldSpace)
            {
                UI = Instantiate(UIprefab, canvas.transform).transform;
                HealthSlider = UI.GetChild(0).GetComponent<Image>();

                break;
            }
        }
        GetComponent<Units_Stats>().OnHealthChanged += OnHealthChanged;


    }
	
	// Update is called once per frame
	void LateUpdate () {
        if(UI != null)
        {
            UI.position = Target.position;
            UI.forward = -cam.forward;
        }
	}
}
