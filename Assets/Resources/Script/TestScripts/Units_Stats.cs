﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Units_Stats : MonoBehaviour
{
    Units_Main Units_MainComponent;

    public int maxHealth = 100;
    public int currentHealth { get; private set; }

    public event System.Action<int, int> OnHealthChanged;

    void Awake()
    {
        currentHealth = maxHealth;
    }


    void Start()
    {
        Units_MainComponent = GetComponent<Units_Main>();
    }


    public void DamageDone(int damage)
    {
        currentHealth -= damage;

        Debug.Log("You Have done" + damage + "damage");
        if(OnHealthChanged != null)
        {
            OnHealthChanged(maxHealth, currentHealth);
        }
    }

    public void Health()
    {
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
    }

    public void Update()
    {
        Health();
    }
}
