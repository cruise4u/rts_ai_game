﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;



public class Units_CTest : MonoBehaviour
{

    Units_Stats CharacterStats;
    Units_Stats TargetStats;
    Units_UI CharactersUI;
    TestWeapon Weapon;
    NavMeshAgent SpecialAgent;

    private  Animator AC;
    private GameObject Self;
    private GameObject Friendly;
    private GameObject Enemy;
    private GameObject Ground;

    public static List<GameObject> UnitsSelection = new List<GameObject>();

    private float attackrange = new float();

    private float attackSpeed = new float();
    private float attackCooldown = new float();


    // booleans to state different conditions
    private bool IsSelecting = false;
    public static bool selecting;

    // Rectangle components to draw in GUI
    private Texture2D boxTxt;
    private Rect BoxRect;
    private Rect SelectionBox;
    private LineRenderer SquareLine;

    public static Vector3 Select_Mouseposition = new Vector3();
    public static Vector3 DirectionToClick = new Vector3();
    public static Vector3 NewUnitPosition = new Vector3();

    public static RaycastHit hit;
    public static Ray ray;

    // Public Functions..

    public static Rect DrawRect(Vector3 ScreenPos_1, Vector3 ScreenPos_2)
    {
        // Move origin from bottom left to top left
        ScreenPos_1.y = Screen.height - ScreenPos_1.y;
        ScreenPos_2.y = Screen.height - ScreenPos_2.y;

        // Calculate corners
        var topLeft = Vector3.Min(ScreenPos_1, ScreenPos_2);
        var bottomRight = Vector3.Max(ScreenPos_1, ScreenPos_2);

        // Create Rect
        return Rect.MinMaxRect(topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    }

    public static Bounds GetSelectionBounds(Camera viewPortCam, Vector3 SP1, Vector3 SP2)
    {
        var vec1 = viewPortCam.ScreenToViewportPoint(SP1);
        var vec2 = viewPortCam.ScreenToViewportPoint(SP2);
        var min = Vector3.Min(vec1, vec2);
        var max = Vector3.Max(vec1, vec2);

        min.z = viewPortCam.nearClipPlane;
        max.z = viewPortCam.farClipPlane;

        var boundsCreate = new Bounds();

        boundsCreate.SetMinMax(min, max);
        return boundsCreate;
    }

    void ContainsOnList()
    {
        if (CheckWithinRect(Self) || RaySelect(Self))
        {
            UnitsSelection.Add(Self);
        }

        if (UnitsSelection.Contains(Self) && Input.GetMouseButton(0))
        {
            UnitsSelection.Remove(Self);
        }

        // And consequently highlighted..
        if (UnitsSelection.Contains(Self))
        {
            SquareLine.enabled = true;
        }
        else
        {
            SquareLine.enabled = false;
        }



    }

    // Movement functions 

    void GoToPoint()
    {
        if (UnitsSelection.Contains(Self))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000))
            {
                if (hit.collider == Ground.GetComponent<Collider>() || Friendly.GetComponent<Collider>())
                {
                    AgentMove(hit.point);
                }
                else if (hit.collider == Enemy.GetComponent<Collider>())
                {
                    AgentMove(hit.point);
                }
            }
        }
    }

    void Attack()
    {
        if(attackCooldown <= 0.0f)
        {
                Weapon.AttackMotion();
                TargetStats.DamageDone(25);
                attackCooldown = 1f / attackSpeed;
        }
    }

    // A.I functionality

    void AgentMove(Vector3 point)
    {
        SpecialAgent.SetDestination(point);
    }


    // Selection functions..

    private bool RaySelect(GameObject InteractableObj)
    {
        if (!IsSelecting)
            return false;

        var boolean = new bool();

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider == InteractableObj.GetComponent<Collider>())
            {
                boolean = true;
            }
            else
            {
                boolean = false;
            }
        }

        return boolean;
    }

    private bool CheckWithinRect(GameObject Game_Obj)
    {
        if (!IsSelecting)
            return false;

        var camera = Camera.main;
        var viewportBounds =
           GetSelectionBounds(camera, Select_Mouseposition, Input.mousePosition);

        return viewportBounds.Contains(
            camera.WorldToViewportPoint(Game_Obj.transform.position));

    }


   public GameObject IsDestroyed(GameObject target)
    {
        if (target != null)
        {
            target = GameObject.FindGameObjectWithTag("Team 2");
        }
        else
        {
            target = null;
        }
        return target;
    }

    public void TeamManager()
    {
        if (gameObject == GameObject.FindGameObjectWithTag("Team 1"))
        {
            Enemy = GameObject.FindGameObjectWithTag("Team 2");
            Friendly = GameObject.FindGameObjectWithTag("Team 1");
            Ground = GameObject.FindGameObjectWithTag("Plane");
        }
        else
        {
            Enemy = GameObject.FindGameObjectWithTag("Team 1");
            Friendly = GameObject.FindGameObjectWithTag("Team 2");
            Ground = GameObject.FindGameObjectWithTag("Plane");
        }
    }

    void Awake()
    {
        TeamManager();
        Self = gameObject;
        TargetStats = Enemy.GetComponent<Units_Stats>();
        SpecialAgent = GetComponent<NavMeshAgent>();
        CharacterStats = GetComponent<Units_Stats>();
        CharactersUI = GetComponent<Units_UI>();
        Weapon = GetComponent<TestWeapon>();
        AC = GetComponent<Animator>();
        SquareLine = GetComponent<LineRenderer>();
    }

    void Start()
    {
        attackCooldown = 1f;
        attackSpeed = 0.5f;
       /* attackrange = 3.0f;*/ // equiv : Melee
        attackrange = 12.0f;
        boxTxt = new Texture2D(1, 1);
    }



    void Update()
    {
        TeamManager();
        attackCooldown -= Time.deltaTime;

        // If we press the left mouse button, save mouse location and begin selection
        if (Input.GetMouseButtonDown(0))
        {
            IsSelecting = true;
            if (IsSelecting == true)
            {
                Select_Mouseposition = Input.mousePosition;
            }
        }

        ContainsOnList();

        // If we have our unit selected and press Right-Click Mouse Button on a target..

        if (Input.GetMouseButtonDown(1))
        {
            GoToPoint();
        }

        if (Input.GetMouseButtonUp(0))
        {
            IsSelecting = false;
        }


        if(IsDestroyed(Enemy))
        {
            if (Vector3.Distance(Self.transform.position, Enemy.transform.position) <= attackrange && hit.collider == Enemy.GetComponent<Collider>())
            {
                Attack();
            }
        }



    }

    // GUI
    void OnGUI()
    {
        if (IsSelecting == true)
        {
            SelectionBox = DrawRect(Select_Mouseposition, Input.mousePosition);
            GUI.DrawTexture(SelectionBox, boxTxt);
        }


    }






}

