﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TestWeapon : MonoBehaviour
{

    Units_CTest MasterObject;

    public GameObject Sphere;

    private Vector3 Master_DirectionUp;

    private Vector3 Master_DirectionFwd;

    private Quaternion RotateSphere;
    

    public void AttackMotion()
    {
        Sphere.transform.localPosition = RotateSphere.eulerAngles * Mathf.Deg2Rad;
    }


    void Start()
    {
        Master_DirectionUp = Vector3.up * Time.deltaTime;
        Master_DirectionFwd = Vector3.left * Time.deltaTime;

        RotateSphere = Quaternion.FromToRotation(Master_DirectionUp, Master_DirectionFwd);
      
    }






}
